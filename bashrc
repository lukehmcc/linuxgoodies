# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific environment
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]
then
    PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# User specific aliases and functions
if [ -d ~/.bashrc.d ]; then
	for rc in ~/.bashrc.d/*; do
		if [ -f "$rc" ]; then
			. "$rc"
		fi
	done
fi

unset rc

# aliases
alias atom="flatpak run io.atom.Atom"
alias adb="~/devStuff/Android/Sdk/platform-tools/adb"
alias code="flatpak run com.visualstudio.code-oss"
alias flutter="$HOME/devStuff/flutter/bin/flutter"
alias dart="$HOME/devStuff/flutter/bin/dart"
alias nas="ssh user@73.227.103.121"
alias start-phone="/home/covalent/devStuff/Android/Sdk/emulator/emulator -avd Pixel_4_XL_API_30"

#and exports
export ANDROID_HOME=$HOME/devStuff/Android/Sdk
export PATH=$PATH:$ANDROID_HOME/platform-tools
export PATH=$PATH:$HOME/.cargo/bin
export MOZ_ENABLE_WAYLAND=1
export PATH=/home/covalent/devStuff/glab/bin:$PATH
